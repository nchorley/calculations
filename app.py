import datetime
import logging
import operator
import traceback

import flask

app = flask.Flask(__name__)

operations = {
    'add': operator.add,
    'subtract': operator.sub,
    'multiply': operator.mul,
    'divide': operator.truediv,
}


@app.route('/<operation_name>', methods={'GET'})
def on_request(operation_name):
    request_id = flask.request.form['id']
    a = flask.request.form['a']
    b = flask.request.form['b']

    app.logger.info('[{}] Request: {} {} {}'.format(
        request_id,
        operation_name,
        a,
        b,
    ))

    try:
        operation = operations[operation_name]
        result = operation(int(a), int(b))
        app.logger.info('[{}] Result: {}'.format(request_id, result))
        response = {'result': result}
    except Exception as e:
        log_traceback(request_id, traceback.format_exc())
        response = {'error': str(e)}

    return flask.jsonify(**response)


def log_traceback(request_id, tb):
    for line in tb.split('\n'):
        app.logger.error('[{}] {}'.format(request_id, line))


def configure_logging():
    handler = logging.FileHandler(
        filename='app-{}.log'.format(datetime.date.today())
    )
    handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s %(message)s'))
    app.logger.addHandler(handler)
    app.logger.setLevel(logging.DEBUG)


if __name__ == '__main__':
    configure_logging()
    app.run()
