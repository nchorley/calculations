import argparse
import functools
import random

import requests

operations = ('add', 'subtract', 'multiply', 'divide')


def get_arguments():
    parser = argparse.ArgumentParser(description='Send random calculation requests')
    parser.add_argument(
        '-n', metavar='N', type=int, default=1, help='make N requests (default: 1)'
    )
    parser.add_argument(
        '--min',
        metavar='MIN',
        type=int,
        default=1,
        help='use MIN as the minimum for random values (default: 1)',
    )
    parser.add_argument(
        '--max',
        metavar='MAX',
        type=int,
        default=10,
        help='use MAX as the minimum for random values (default: 10)',
    )
    parser.add_argument(
        '--host',
        metavar='HOST',
        type=str,
        default='localhost',
        help='connect to application running on HOST (default: localhost)',
    )
    parser.add_argument(
        '--port',
        metavar='PORT',
        type=int,
        default=5000,
        help='connect to application running on PORT (default: 5000)',
    )

    return parser.parse_args()


if __name__ == '__main__':
    arguments = get_arguments()
    random_value = functools.partial(random.randint, a=arguments.min, b=arguments.max)

    for i in range(arguments.n):
        operation = random.choice(operations)
        a = random_value()
        b = random_value()
        response = requests.get(
            'http://{}:{}/{}'.format(arguments.host, arguments.port, operation),
            data={'a': a, 'b': b, 'id': i},
        )

        body = response.json()
        print('[{}] Request: {} {} {}'.format(i, operation, a, b))
        try:
            print('[{}] Result: {}'.format(i, body['result']))
        except KeyError:
            print('[{}] Error: {}'.format(i, body['error']))
