The web application and client used for my
[intro to grep](http://nicky.xyphias.com/talks/intro-to-grep.pdf) talk.

This is written in Python 3. The web app uses [Flask](http://flask.pocoo.org/)
and the client uses [Requests](http://www.python-requests.org/en/latest/).
